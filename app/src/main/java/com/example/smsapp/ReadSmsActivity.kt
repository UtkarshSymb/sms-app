package com.example.smsapp

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class ReadSmsActivity : AppCompatActivity(), SmsBroadcastReceiver.SMSReceiveListener {
    private var SMS_PERMISSION_CODE: Int = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_read_sms)
        var permissionGranted = isPermissionGranted()
        if (permissionGranted) {

        } else {
            requestSMSPermission()
        }


    }


    private fun isPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_SMS
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestSMSPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.READ_SMS
            )
        ) {
            //TODO show a dialog box before asking sms permission
        }
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.READ_SMS),
            SMS_PERMISSION_CODE
        )
    }

    override fun onTextReceived(smsBody: String, smsSender: String?) {
        Toast.makeText(this, smsBody, Toast.LENGTH_LONG).show()
    }


}