package com.example.smsapp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.Telephony
import android.telephony.SmsMessage
import android.util.Log

class SmsBroadcastReceiver : BroadcastReceiver() {
    private val TAG = "SmsBroadcastReceiver"
    private lateinit var listener: SMSReceiveListener
    fun initSMSListener(receiver: SMSReceiveListener) {
        this.listener = receiver

    }

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action.equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)) {
            var smsSender: String? = ""
            var smsBody = ""
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                for (smsMessage in Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
                    smsSender = smsMessage.displayOriginatingAddress
                    smsBody += smsMessage.messageBody
                }

            } else {
                var smsBundle: Bundle? = intent?.extras
                if (smsBundle != null) {
                    val pdus =
                        smsBundle["pdus"] as Array<Any>?
                    if (pdus == null) { // Display some error to the user
                        Log.e(TAG, "SmsBundle had no pdus key")
                        return
                    }
                    val messages: Array<SmsMessage?> = arrayOfNulls<SmsMessage>(pdus.size)
                    for (i in messages.indices) {
                        messages[i] = SmsMessage.createFromPdu(pdus[i] as ByteArray)
                        smsBody += messages[i]?.messageBody
                    }
                    smsSender = messages[0]?.originatingAddress
                }

            }

            if (listener != null) {
                listener.onTextReceived(smsBody, smsSender)
            }
        }

    }

    interface SMSReceiveListener {
        fun onTextReceived(smsBody: String, smsSender: String?)


    }

}