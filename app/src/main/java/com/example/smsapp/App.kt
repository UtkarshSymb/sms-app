package com.example.smsapp

import android.app.Application
import android.content.IntentFilter
import android.provider.Telephony

class App : Application() {
    private lateinit var smsBroadcastReceiver: SmsBroadcastReceiver

    override fun onCreate() {
        super.onCreate()
        registerReceiver(
            smsBroadcastReceiver,
            IntentFilter(Telephony.Sms.Intents.SMS_CB_RECEIVED_ACTION)
        )
    }

    override fun onTerminate() {
        unregisterReceiver(smsBroadcastReceiver)
        super.onTerminate()
    }
}