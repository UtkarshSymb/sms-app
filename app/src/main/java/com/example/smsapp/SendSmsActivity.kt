package com.example.smsapp


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.telephony.SmsManager
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_send_sms.*


@SuppressLint("ByteOrderMark")
class SendSmsActivity : AppCompatActivity() {

    private val TAG = "PermissionDemo"
    private val RECORD_REQUEST_CODE = 101

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_sms)
        setupPermissions()
        send.setOnClickListener {
            setupPermissions()
            //Getting intent and PendingIntent instance
            //Get the SmsManager instance and call the sendTextMessage method to send message
            if (msg.text.toString().isNotEmpty() && number.text.toString().isNotEmpty()) {
                val sms: SmsManager = SmsManager.getDefault()
                val intent = Intent(applicationContext, SendSmsActivity::class.java)
//                val pendingIntent = PendingIntent.getActivity(applicationContext, 0, intent, 0)
                val pendingIntent = PendingIntent.getBroadcast(
                    this,
                    0,
                    Intent("SMS_SENT"),
                    PendingIntent.FLAG_UPDATE_CURRENT
                )
                val smsSentReceiver: BroadcastReceiver = object : BroadcastReceiver() {
                    override fun onReceive(context: Context?, intent: Intent?) {
                        var result = ""
                        when (resultCode) {
                            Activity.RESULT_OK -> result = "SMS sent Successfully"
                            SmsManager.RESULT_ERROR_GENERIC_FAILURE -> result =
                                "SMS not sent"
                            SmsManager.RESULT_ERROR_RADIO_OFF -> result =
                                "Radio off"
                            SmsManager.RESULT_ERROR_NULL_PDU -> result =
                                "No PDU defined"
                            SmsManager.RESULT_ERROR_NO_SERVICE -> result =
                                "No service"
                        }
                        Toast.makeText(
                            applicationContext, result,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
                registerReceiver(smsSentReceiver, IntentFilter("SMS_SENT"))

                try {
                    sms.sendTextMessage(
                        number.text.toString(),
                        null,
                        msg.text.toString(),
                        pendingIntent,
                        null
                    )
                    number.text = null
                    msg.text = null
                } catch (e: Exception) {
                    Toast.makeText(
                        applicationContext, "Message Not Sent!",
                        Toast.LENGTH_LONG
                    ).show()
                    e.printStackTrace()
                }


            } else {
                if (msg.text.toString().isEmpty()) {
                    Toast.makeText(
                        applicationContext, "please write Message",
                        Toast.LENGTH_LONG
                    ).show()

                } else if (number.text.toString().isEmpty()) {
                    Toast.makeText(
                        applicationContext, "please input number",
                        Toast.LENGTH_LONG
                    ).show()
                }

            }
        }

    }

    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.SEND_SMS
        )

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Permission to Send sms denied")
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.SEND_SMS
                )
            ) {
                makeRequest()
//                val builder = AlertDialog.Builder(this)
//                builder.setMessage("Permission to access the Send_SMS is required for this app to Send SMS.")
//                    .setTitle("Permission required")
//
//                builder.setPositiveButton(
//                    "OK"
//                ) { dialog, id ->
//                    Log.i(TAG, "Clicked")
//                    makeRequest()
//                }
//
//                val dialog = builder.create()
//                dialog.show()
            } else {
                makeRequest()
            }
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.SEND_SMS),
            RECORD_REQUEST_CODE
        )
    }


//    override fun onRequestPermissionsResult(
//        requestCode: Int,
//        permissions: Array<String>,
//        grantResults: IntArray
//    ) {
//        var requestCode = requestCode
//        when (requestCode) {
//            RECORD_REQUEST_CODE -> {
//
//                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
//
//                    Toast.makeText(this, "Permission has been denied by user", Toast.LENGTH_SHORT)
//                        .show()
//                } else {
//                    Toast.makeText(this, "Permission has been granted by user", Toast.LENGTH_SHORT)
//                        .show()
//                }
//            }
//            else->{
//                Toast.makeText(this, "Something bad happened", Toast.LENGTH_SHORT)
//                    .show()
//
//            }
//        }
//    }﻿
}
